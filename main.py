from pycoproc import Pycoproc
from network import WLAN
import time
import gc

gc.enable()

## Code To Do With WIFI Related Settings
## WiFi Is Not Persisted

wifi_access = WLAN(mode=WLAN.STA)
WIFI_SSID = ""
WIFI_PASSWORD = ""
WIFI_USERNAME = ""


time.sleep(2)
print("Connecting to Wifi. SSID {}".format(WIFI_SSID))
wifi_access.connect(WIFI_SSID,auth=(WLAN.WPA2,WIFI_PASSWORD))
time.sleep(15)

if wifi_access.isconnected() == True:
    print("I am Connected to WiFi and My WiFI Details are: {}".format(wifi_access.ifconfig()))

## Figure out the Expansion Board we are using (Means We only have to manage one codebase rather than 2)
## https://forum.pycom.io/topic/2469/easy-way-to-tell-what-expansion-board-is-in-use/3

try:
    expansion_identififer = Pycoproc()
    product_id = expansion_identififer.read_product_id()

    if product_id == 61458:
        print("I Am a Pysense so you should run Pysense Code.")

    elif product_id == 61459:
        print("I Am a Pytrack so will run the Pytrack Code.")
        import pytrack_sensors

    else:
        print("The board that I am hasn't been configured")

except:
    print("No Pysense or PyTrack in Use")

import mqtt_mosquito

while True:
    print("Waiting for Acutation Data")
    time.sleep(10)
    mqtt_mosquito.MQTT_CLIENT.check_msg()
