from sensor_L76GNSS import L76GNSS
from sensor_LIS2HH12 import LIS2HH12
from pytrack import Pytrack
import reekoh
import time

py = Pytrack()
gps = L76GNSS(py, timeout=10)
accpitchyaw = LIS2HH12(py)
PYTRACK_DATA = {}
reekoh.connect()

while (True):
    PYTRACK_DATA["coord"] = gps.coordinates(debug=True)
    PYTRACK_DATA["accel"] = accpitchyaw.acceleration()
    PYTRACK_DATA["pitch"] = accpitchyaw.pitch()
    PYTRACK_DATA["roll"] = accpitchyaw.roll()
    reekoh.publish(PYTRACK_DATA)
