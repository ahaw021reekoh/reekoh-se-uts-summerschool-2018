import pycom
pycom.heartbeat(False)

def actuate_led(ledcolor):
    print("Actuation of LED Requested - Color is {}".format(ledcolor))

    if type(ledcolor) == bytes:
        ledcolor = ledcolor.decode().upper()

    if type(ledcolor) == str:
        ledcolor = ledcolor.upper()

    if ledcolor == "RED":
        pycom.rgbled(0xff0000)

    elif ledcolor == "BLUE":
        pycom.rgbled(0x0000ff)

    elif ledcolor == "CYAN":
        pycom.rgbled(0x00ffff)

    elif ledcolor == "YELLOW":
        pycom.rgbled(0x7f7f00)

    elif ledcolor == "PURPLE":
        pycom.rgbled(0xa020f0)

    elif ledcolor == "GREEN":
        pycom.rgbled(0x007f00)

    elif ledcolor == "VIOLET":
        pycom.rgbled(0xdda0dd)

    elif ledcolor == "PINK":
        pycom.rgbled(0xff3333)

    else :
        print("Cannot Actuate")
