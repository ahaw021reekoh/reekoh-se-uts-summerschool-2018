from pysense import Pysense
from sensor_LIS2HH12 import LIS2HH12
from sensor_SI7006A20 import SI7006A20
from sensor_LTR329ALS01 import LTR329ALS01
from sensor_MPL3115A2 import MPL3115A2,ALTITUDE,PRESSURE
import time
import reekoh
import ujson

py = Pysense()
mp = MPL3115A2(py,mode=ALTITUDE) # Returns height in meters. Mode may also be set to PRESSURE, returning a value in Pascals
si = SI7006A20(py)
lt = LTR329ALS01(py)
li = LIS2HH12(py)
PYSENSE_DATA = {}

reekoh.connect()

while True:
    PYSENSE_DATA["temperature"] = mp.temperature()
    PYSENSE_DATA["altitude"]  = mp.altitude()
    mpp = MPL3115A2(py,mode=PRESSURE)
    PYSENSE_DATA["pressure"]  = mpp.pressure()

    PYSENSE_DATA["si_temperature"] = si.temperature()
    PYSENSE_DATA["humidity"] = si.humidity()
    PYSENSE_DATA["dewpoint"] = si.dew_point()
    PYSENSE_DATA["light"] = lt.light()

    PYSENSE_DATA["acceleration"] = li.acceleration()
    PYSENSE_DATA["roll"] = li.roll()
    PYSENSE_DATA["pitch"] = li.pitch()
    PYSENSE_DATA["batteryVoltage"] = py.read_battery_voltage()
    reekoh.publish(PYSENSE_DATA)
    time.sleep(10)
