
from mqtt import MQTTClient
import ujson
import hashlib
from network import WLAN
import binascii
import actuate

wifi_access = WLAN()

## Configs for Reekoh MQTT Broker

MQTT_BROKER_IP = ""
MQTT_BROKER_PORT = 8080
DATA_TOPIC = ""
COMMAND_TOPIC = ""


## Create a DEVICEID Based on the MAC of the WIFI Card.
## This is then hashed using SHA1 to create a derivative ID. Because you know IoT security is a thing :D

SHA1 = hashlib.sha1()
SHA1.update(wifi_access.mac())
DEVICE_ID = binascii.hexlify(SHA1.digest())
print("The Reekoh device id will be: {}".format(DEVICE_ID.decode()))

## Create and MQTT Client Object

MQTT_CLIENT = MQTTClient(DEVICE_ID,MQTT_BROKER_IP,MQTT_BROKER_PORT)


def actuate_LED(topic, msg):
   actuate.actuate_led(msg)

def connect():
    MQTT_CLIENT.connect()
    MQTT_CLIENT.set_callback(actuate_LED)
    MQTT_CLIENT.subscribe(topic=COMMAND_TOPIC)


def disconnect():
    MQTT_CLIENT.disconnect()

def publish(MQTT_PAYLOAD_BODY):
    MQTT_REEKOH_JSON = ujson.dumps(MQTT_PAYLOAD_BODY)
    print("Sending: {}".format(MQTT_REEKOH_JSON))
    MQTT_CLIENT.publish(topic=DATA_TOPIC,msg=MQTT_REEKOH_JSON)

connect()
